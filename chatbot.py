#!/usr/bin/env python
# -*- coding: utf-8 -*-

import lilAIML as aiml
import marshal
import mgoogle
import mgoogleimage
import mporn
import mtodo
import mtrans
import mtwitter
import muva
import mwiki
import re
import sys
import uuid
import downloader
import cache

noPIL = False
try:
	from PIL import Image
except ImportError:
    noPIL = True


# Configuración del bot
sessionDataFile = "datos.ses"
downloads = "./downs/"
downloadSizeLimit = 1048576 # 1MiB

# Ficheros ignorados por ser ineficientes:
# "aparienciaFisica.aiml"
# "estadoAnimo.aiml"
# "atributosPsicologicos.aiml" 

aimlFiles = ["adjetivos.aiml"
,"apelativos.aiml"
,"astrologia.aiml"
,"chistes.aiml"
,"conectores.aiml"
,"edad.aiml"
,"familia.aiml"
,"genero.aiml"
,"google.aiml"
,"infoBot.aiml"
,"infoUsuario.aiml"
,"insultos.aiml"
,"lilezek.aiml"
,"love.aiml"
,"nacionalidades.aiml"
,"nombre.aiml"
,"nombres.aiml"
,"peticiones.aiml"
,"preguntas.aiml"
,"profesiones.aiml"
,"respuestaGenerales.aiml"
,"rule34.aiml"
,"rules.aiml"
,"saludos.aiml"
,"sino.aiml"
,"sinRespuestas.aiml"
,"todo.aiml"
,"traductor.aiml"
,"twitter.aiml"
,"uva.aiml"
,"valve.aiml"
,"wiki.aiml"]


class chatbot:
	def __init__(self):

		self.k = aiml.Kernel()
		for f in aimlFiles:
			self.k.learn(f)
		self.k.setBotPredicate("name","Cristal")
		self.k.setBotPredicate("age","17")
		self.k.setBotPredicate("gender","mujer")
		self.k.setBotPredicate("location","Murcia")
		self.k.setBotPredicate("birthday","20")
		self.k.setBotPredicate("birthmonth","Octubre")
		self.k.setBotPredicate("favoriteColor","rosa")
		self.k.setBotPredicate("footballteam","Real Betis")
		self.k.setBotPredicate("civilState","lesbiana")
		self.k.setBotPredicate("favouriteLanguage","Python")
		self.k.setBotPredicate("favoriteAuthor","Isaac Asimov")
		self.k.setBotPredicate("favoriteArtist","Chuck Norris")
		self.k.setBotPredicate("favoriteActress","Julia Roberts")
		self.k.setBotPredicate("favoriteSport","zekiball")

		print "Loading modules"
		uv = muva.moduleUVa()
		tm = mtodo.moduleTodo()
		go = mgoogle.moduleGoogle()
		gi = mgoogleimage.moduleGoogleImage()
		wi = mwiki.moduleWiki()
		mt = mtrans.moduleTranslate()
		ep = mporn.modulePorn()
		tw = mtwitter.moduleTwitter()
		self.k.registerModule(uv)
		self.k.registerModule(tm)
		self.k.registerModule(go)
		self.k.registerModule(gi)
		self.k.registerModule(wi)
		self.k.registerModule(mt)
		self.k.registerModule(ep)
		self.k.registerModule(tw)
		print "Ready"

	def respond(self, peer, msg):
		sentence = self.k.respond(msg,peer)
		return sentence

	def storeAllSessions(self):
		session = self.k.getSessionData()
		sessionFile = file(sessionDataFile, "wb")
		marshal.dump(session, sessionFile)
		sessionFile.close()

	def loadAllSessions(self):
		try:
			sessionFile = file(sessionDataFile, "rb")
			session = marshal.load(sessionFile)
			sessionFile.close()
		except IOError as e:
			return
		except EOFError as e:
			return 
		for k,v in session.items():
			self.loadSession(k,v)

	def loadSession(self, sesName, sesDict):
		for pred,value in sesDict.items():
			self.k.setPredicate(pred, value, sesName)


d = downloader.Downloader(folder=downloads)
def AdaptorDownloadImage(url):
	return d.downloadImage(url)
cDownloadImage = cache.Cache(downloads,AdaptorDownloadImage)

urlCheck = re.compile("(https?:\\/\\/)([^\\/:\\s]*)(\\.\\w+)(:\\d+)?(\\/[^\\s]*)?")
def checkForImages(msg):
	m = urlCheck.search(msg)
	if (m):
		return cDownloadImage(m.group(0))
	return None

if (__name__ == "__main__"):
	bot = chatbot()
	bot.loadAllSessions()
	line = ""
	try:
		for line in iter(sys.stdin.readline,''):
			r = bot.respond("Bob",line)
			try:
				x = checkForImages(r)
				if (x):
					im = Image.open(x)
					im.show()
			except downloader.SizeLimitError as e:
				print "Imagen demasiado grande (al menos "+("%.2f" % (e.getSize()/float(1048576)))+"MiB)"
			print "> " + r
	except KeyboardInterrupt as e:
		cache.flushAllCaches()
		bot.storeAllSessions()