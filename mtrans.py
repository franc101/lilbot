#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lilAIML import module;
import Queue
import goslate
import urllib2

# Variables a leer de los aiml:
translateLanguage = "translate_language" # Indice del lenguaje a traducir
translateText = "translate_text" # Texto a traducir

class moduleTranslate(module.IModule):
    def __init__(self):
        self.q = Queue.Queue()
        self.trans = goslate.Goslate()

    def execute(self, kern, peer):
        session = kern.getSessionData(peer)
        if (translateText in session.keys() and translateLanguage in session.keys()):
            val1 = session[translateText]
            val2 = session[translateLanguage]
            kern.setPredicate(translateLanguage,None,peer)
            kern.setPredicate(translateText,None,peer)
            try:
                if (val1 != "" and val2 != ""):
                    self.q.put(self.trans.translate(val1,val2))
                    return True
            except urllib2.HTTPError:
                pass
            except goslate.Error:
                pass
        self.q.put("No sé cómo se traduce eso.")
        return True
    
    def readOutput(self):
        if (self.q.empty()):
            return None
        return self.q.get()
    
    def getToken(self):
        return "translate"
