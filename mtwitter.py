#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lilAIML import module;
import tweepy
import Queue
import imp
import twitterAUTH

# Configuración
# CONSUMER_KEY = ''
# CONSUMER_SECRET = ''
# ACCESS_KEY = ''
# ACCESS_SECRET = ''

CONSUMER_KEY = twitterAUTH.CONSUMER_KEY
CONSUMER_SECRET = twitterAUTH.CONSUMER_SECRET
ACCESS_KEY = twitterAUTH.ACCESS_KEY
ACCESS_SECRET = twitterAUTH.ACCESS_SECRET

# Variables a leer de los aiml:
twitText = "text" # Texto a emitir por twit


class moduleTwitter(module.IModule):
    def __init__(self):
        self.q = Queue.Queue()
        auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
        auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
        self._api = tweepy.API(auth)

    def execute(self, kern, peer):
        session = kern.getSessionData(peer)
        if (twitText in session.keys()):
            val = session[twitText]
            kern.setPredicate(twitText,None,peer)
            if (val != ""):
                self.sendTwit(val)
                self.q.put(u"https://twitter.com/botcristal")
                return True    
        print "WARNING: No text for twitter module"
        return False
    
    def readOutput(self):
        if (self.q.empty()):
            return None
        return self.q.get()
    
    def getToken(self):
        return "twitter"
    
    def sendTwit(self, text):
        try:
            self._api.update_status(text)
        except tweepy.TweepError as e:
            pass
        

if __name__=="__main__":
    m = moduleTwitter()
    m.sendTwit("hola")