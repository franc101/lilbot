#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lilAIML import module;
import Queue
import urllib2
import json
from cache import Cache
from datetime import datetime

# Variables a leer de los aiml:
problemStr = "problem" 

# Tabla de conversiones:

veredict = {
    10 : "Submission error",
    15 : "Can't be judged",
    20 : "In queue",
    30 : "Compile error",
    35 : "Restricted function",
    40 : "Runtime error",
    45 : "Output limit",
    50 : "Time limit",
    60 : "Memory limit",
    70 : "Wrong answer",
    80 : "PresentationE",
    90 : "Accepted ",
}

def submisionToText(sub):
    result = str(cProblemNumber(str(sub[1]))) + " - " + veredict[sub[2]] + " - " + datetime.fromtimestamp(sub[4]).strftime('%Y-%m-%d %H:%M:%S')
    return result

def problemInfo(problemID):
    request = urllib2.urlopen("http://uhunt.felix-halim.net/api/p/id/"+problemID)
    result = json.loads(request.read())
    request.close()
    return result

def problemNumber(problemID):
    return problemInfo(problemID)["num"]

cProblemNumber = Cache("./downs", problemNumber)

def username2Userid(username):
    request = urllib2.urlopen("http://uhunt.felix-halim.net/api/uname2uid/"+username)
    result = request.read();
    request.close()
    return result

cUsername2Userid = Cache("./downs", username2Userid)

def getSubmissions(userid, count = 4):
    request = urllib2.urlopen("http://uhunt.felix-halim.net/api/subs-user-last/"+userid+"/"+str(count))
    print request.info().getdate('last-modified')
    data = json.loads(request.read())
    request.close()
    try:
        result = data["name"]+": "
        for s in reversed(data["subs"]):
            result += submisionToText(s)+"\n"
        return result
    except IndexError:
        return u"Usuario o información incorrecta"

class moduleUVa(module.IModule):
    def __init__(self):
        self.q = Queue.Queue()

    def execute(self, kern, peer):
        session = kern.getSessionData(peer)
        if (problemStr in session.keys()):
            val = session[problemStr].strip()
            kern.setPredicate(problemStr,None,peer)
            if (val == ""):
                    self.q.put(u"Problema no indicado")
            else:
                try:
                    int(val)
                    self.q.put(u"Text: http://uva.onlinejudge.org/external/"+val[:len(val)-2]+"/"+val+".html\n")
                    self.q.put(u"Discuss: http://acm.uva.es/board/search.php?keywords="+val+"\n")
                    self.q.put(u"udebug: http://www.udebug.com/UVa/"+val)
                except ValueError:
                    self.q.put(getSubmissions(cUsername2Userid(val)))

        return True
    
    def readOutput(self):
        if (self.q.empty()):
            return None
        return self.q.get()
    
    def getToken(self):
        return "uva"

if __name__ == "__main__":
    print getSubmissions(cUsername2Userid("lilEzek"))
