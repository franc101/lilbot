#!/usr/bin/env python
# -*- coding: utf-8 -*-

import copy
import random

width = 14
height = 16

charBlock = u"﻿﻿\U000025FD"
charEmpty = u"\U000025FC"
charPiece = u"\U0000274E"

class PiecePrototype:
	def Flip(self):
		raise NotImplementedError("Flip not implemented")
	def Unflip(self):
		raise NotImplementedError("Unflip not implemented")
	def Print(self):
		raise NotImplementedError("Print not implemented")

class PieceI(PiecePrototype):
	_rotations = [[charPiece*4],[charPiece]*4]
	_offsets = [(-1,0),(0,-1)]
	_rot = 0

	def Flip(self):
		self._rot = (self._rot + 1)%2

	def Unflip(self):
		self._rot = (self._rot - 1)%2

	def Print(self):
		return self._offsets[self._rot],self._rotations[self._rot]

class PieceO(PiecePrototype):
	_rotations = [[charPiece*2]*2]
	_offsets = [(0,0)]
	_rot = 0

	def Flip(self):
		pass

	def Unflip(self):
		pass

	def Print(self):
		return self._offsets[self._rot],self._rotations[self._rot]

class PieceL(PiecePrototype):
	_rotations = [[charPiece+charEmpty,charPiece+charEmpty,charPiece*2],
					[charEmpty*2+charPiece,charPiece*3],
					 [charPiece*2,charEmpty+charPiece,charEmpty+charPiece],
					 [charPiece*3,charPiece+charEmpty*2]]
	_offsets = [(0,0),(-1,0),(-1,0),(-1,1)]
	_rot = 0

	def Flip(self):
		self._rot = (self._rot + 1)%4

	def Unflip(self):
		self._rot = (self._rot - 1)%4

	def Print(self):
		return self._offsets[self._rot],self._rotations[self._rot]

class PieceT(PiecePrototype):
	_rotations = [[charEmpty+charPiece+charEmpty,charPiece*3],
					[charEmpty+charPiece,charPiece*2,charEmpty+charPiece],
					 [charPiece*3,charEmpty+charPiece+charEmpty],
					 [charPiece+charEmpty,charPiece*2,charPiece+charEmpty]]
	_offsets = [(0,0),(0,0),(0,1),(1,0)]
	_rot = 0

	def Flip(self):
		self._rot = (self._rot + 1)%4

	def Unflip(self):
		self._rot = (self._rot - 1)%4

	def Print(self):
		return self._offsets[self._rot],self._rotations[self._rot]

class PieceZ(PiecePrototype):
	_rotations = [[charPiece*2+charEmpty,charEmpty+charPiece*2],
					[charEmpty+charPiece,charPiece*2,charPiece+charEmpty]]
	_offsets = [(-1,0),(0,0)]
	_rot = 0

	def Flip(self):
		self._rot = (self._rot + 1)%2

	def Unflip(self):
		self._rot = (self._rot - 1)%2

	def Print(self):
		return self._offsets[self._rot],self._rotations[self._rot]

class PieceS(PiecePrototype):
	_rotations = [[charEmpty+charPiece*2,charPiece*2+charEmpty],
					[charPiece+charEmpty,charPiece*2,charEmpty+charPiece]]
	_offsets = [(-1,0),(0,0)]
	_rot = 0

	def Flip(self):
		self._rot = (self._rot + 1)%2

	def Unflip(self):
		self._rot = (self._rot - 1)%2

	def Print(self):
		return self._offsets[self._rot],self._rotations[self._rot]


pieces = [PieceI(),PieceO(),PieceL(),PieceT(),PieceZ(),PieceS()]

class Tetris:

	def __init__(self):
		self._board = []
		for x in xrange(0,height):
			self._board.append([charEmpty]*width)
		self._actPiece = copy.deepcopy(random.choice(pieces))
		self._piecePos = (width//2-1,0)
		self._DrawPiece()
		self._score = 0

	def Print(self):
		result = str(self._score)+u"\n"+charBlock*(width+2)
		for line in self._board:
			result += u"\n"+charBlock+u"".join(line)+charBlock
		result += u"\n"+charBlock*(width+2)
		return result

	def _ClearBoard(self):
		for i in xrange(0,len(self._board)):
			self._board[i] = [charEmpty if x == charPiece else x for x in self._board[i]]

	def _FixPiece(self):
		for i in xrange(0,len(self._board)):
			self._board[i] = [charBlock if x == charPiece else x for x in self._board[i]]
		

	def _DrawPiece(self):
		(offx,offy),piece = self._actPiece.Print()
		y = self._piecePos[1]+offy
		for line in piece:
			x = self._piecePos[0]+offx
			for c in [line[i:i+len(charPiece)] for i in range(0, len(line), len(charPiece))]:
				if (x < 0 or x >= width or y < 0 or y >= height):
					return False
				if (c == charPiece and self._board[y][x] == charBlock):
					return False
				if (c == charPiece):
					self._board[y][x] = c
				x += 1
			y += 1
		return True

	def Step(self):
		oldboard = copy.deepcopy(self._board)
		self._ClearBoard()
		self._piecePos = (self._piecePos[0],1+self._piecePos[1])
		if (not self._DrawPiece()):
			self._board = oldboard
			self._FixPiece()
			self._CheckScore(1)
			self._actPiece = copy.deepcopy(random.choice(pieces))
			self._piecePos = (width//2-1,0)
			return self._DrawPiece()
		return True

	def Next(self):
		s = self.Step()
		while (s and self._piecePos[1] != 0):
			s = self.Step()
		return s

	def _CheckScore(self, points):
		index = -1
		for i in xrange(0,len(self._board)):
			if (all([x == charBlock for x in self._board[i]])):
				index = i
				break
		if (index == -1):
			return
		else:
			self._score += points
			self._board.pop(index)
			self._board.insert(0,[charEmpty]*width)
			self._CheckScore(points+1)


	def Left(self):
		self._ClearBoard()
		self._piecePos = (self._piecePos[0]-1,self._piecePos[1])
		if (not self._DrawPiece()):
			self._ClearBoard()
			self._piecePos = (self._piecePos[0]+1,self._piecePos[1])
			self._DrawPiece()
		return True

	def Right(self):
		self._ClearBoard()
		self._piecePos = (self._piecePos[0]+1,self._piecePos[1])
		if (not self._DrawPiece()):
			self._ClearBoard()
			self._piecePos = (self._piecePos[0]-1,self._piecePos[1])
			self._DrawPiece()
		return True

	def Flip(self):
		self._ClearBoard()
		self._actPiece.Flip()
		if (not self._DrawPiece()):
			self._ClearBoard()
			self._actPiece.Unflip()
			self._DrawPiece()
		return True

	def Save(self):
		pass

	def Load(self):
		pass

def nothing(self):
	return True

ordenes = {	"girar" 	: Tetris.Flip,
			"derecha" 	: Tetris.Right,
			"abajo" 	: Tetris.Step,
			"izquierda" : Tetris.Left, 
			"siguiente" : Tetris.Next,
			"w" 		: Tetris.Flip,
			"d" 		: Tetris.Right,
			"s" 		: Tetris.Step,
			"a" 		: Tetris.Left, 
			"p"			: Tetris.Next,
			""			: nothing,
			"\x1B[A"	: Tetris.Flip,
			"\x1B[B"	: Tetris.Step,
			"\x1B[D"	: Tetris.Left,
			"\x1B[C"	: Tetris.Right,}

if __name__=="__main__":
	t = Tetris()
	print t.Print()
	x = raw_input()
	while (x != "exit"):
		if (not ordenes.has_key(x)):
			print "girar, abajo, derecha, izquierda, siguiente, exit"
		else:
			if (not ordenes[x](t)):
				exit(0)
			print t.Print()
		x = raw_input()
